require 'test_helper'

class WentriesControllerTest < ActionController::TestCase
  setup do
    @wentry = wentries(:one)
    @update = {
      category: "Tests",
      amount: 2500,
      ic: "Income",
    }
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:wentries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create wentry" do
    assert_difference('Wentry.count') do
      post :create, wentry: @update
    end

    assert_redirected_to wentry_path(assigns(:wentry))
  end

  test "should show wentry" do
    get :show, id: @wentry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @wentry
    assert_response :success
  end

  test "should update wentry" do
    patch :update, id: @wentry, wentry: @update
    assert_redirected_to wentry_path(assigns(:wentry))
  end

  test "should destroy wentry" do
    assert_difference('Wentry.count', -1) do
      delete :destroy, id: @wentry
    end

    assert_redirected_to wentries_path
  end
end
