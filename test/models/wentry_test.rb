require 'test_helper'

class WentryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  test "wentry attributes must not be empty" do
    wentry = Wentry.new
    assert wentry.invalid?
    assert wentry.errors[:category].any?
    assert wentry.errors[:amount].any?
    assert wentry.errors[:ic].any?
  end

  test "wentry amount must be positive" do
    wentry = Wentry.new(category: "Another test",
                        ic: 'Income')
    wentry.amount = -1
    assert wentry.invalid?
    assert_equal ["must be greater than or equal to 1"],
      wentry.errors[:amount]

    wentry.amount = 0
    assert wentry.invalid?
    assert_equal ["must be greater than or equal to 1"], 
      wentry.errors[:amount]

    wentry.amount = 1
    assert wentry.valid?
  end
  
end
