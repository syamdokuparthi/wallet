class CreateWentries < ActiveRecord::Migration
  def change
    create_table :wentries do |t|
      t.string :category
      t.integer :amount
      t.string :ic

      t.timestamps null: false
    end
  end
end
