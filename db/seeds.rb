# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Wentry.delete_all

Wentry.create!(category: "Grocery", amount: 12050, ic: 'Expense')
Wentry.create!(category: "Sports", amount: 2200, ic: 'Expense')
Wentry.create!(category: "Others", amount: 7770, ic: 'Expense')
Wentry.create!(category: "Grocery", amount: 2999, ic: 'Expense')
Wentry.create!(category: "Taxi", amount: 500, ic: 'Expense')
Wentry.create!(category: "Salary", amount: 130000, ic: 'Income')