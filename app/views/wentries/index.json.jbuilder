json.array!(@wentries) do |wentry|
  json.extract! wentry, :id, :category, :amount, :ic
  json.url wentry_url(wentry, format: :json)
end
