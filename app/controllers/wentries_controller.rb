class WentriesController < ApplicationController
  before_action :set_wentry, only: [:show, :edit, :update, :destroy]

  # GET /wentries
  # GET /wentries.json
  def index
    if params[:set_locale]
      redirect_to wentries_url(locale: params[:set_locale])
    else
      @wentries = Wentry.where(nil)
      @wentries = @wentries.ic(params[:ic]) if params[:ic].present?
      @wentries = @wentries.from_date(params[:from_date]) if params[:from_date].present?
      @wentries = @wentries.to_date(params[:to_date]) if params[:to_date].present?
    end
    
  end


  # GET /wentries/1
  # GET /wentries/1.json
  def show
  end

  # GET /wentries/new
  def new
    @wentry = Wentry.new
  end

  # GET /wentries/1/edit
  def edit
  end

  # POST /wentries
  # POST /wentries.json
  def create
    @wentry = Wentry.new(wentry_params)

    respond_to do |format|
      if @wentry.save
        format.html { redirect_to @wentry, notice: 'Wentry was successfully created.' }
        format.json { render :show, status: :created, location: @wentry }
      else
        format.html { render :new }
        format.json { render json: @wentry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /wentries/1
  # PATCH/PUT /wentries/1.json
  def update
    respond_to do |format|
      if @wentry.update(wentry_params)
        format.html { redirect_to @wentry, notice: 'Wentry was successfully updated.' }
        format.json { render :show, status: :ok, location: @wentry }
      else
        format.html { render :edit }
        format.json { render json: @wentry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /wentries/1
  # DELETE /wentries/1.json
  def destroy
    @wentry.destroy
    respond_to do |format|
      format.html { redirect_to wentries_url, notice: 'Wentry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wentry
      @wentry = Wentry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wentry_params
      params.require(:wentry).permit(:category, :amount, :ic)
    end
end
