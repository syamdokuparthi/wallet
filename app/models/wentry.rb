class Wentry < ActiveRecord::Base
  
  scope :ic, -> (ic) {where ic: ic}
  scope :from_date, -> (date) {where("created_at >= (?)", Date.civil(date[:year].to_i, date[:month].to_i, date[:day].to_i).to_datetime)}
  scope :to_date, -> (date) {where("created_at <= (?)", Date.civil(date[:year].to_i, date[:month].to_i, date[:day].to_i).to_datetime+1)}
  
  monetize :amount, as: 'price'
  
  validates :amount, numericality: {greater_than_or_equal_to: 1}
  validates :category, :ic, presence: true
  
end
